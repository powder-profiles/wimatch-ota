#!/bin/bash

NTPSERVER=ops.emulab.net

# Install required packages for all Shout/Wimatch components
sudo apt-get update
sudo apt-get install -y python3-pip python3-numpy python3-scipy python3-matplotlib python3-daemon python3-protobuf python3-h5py python3-z3
pip3 install parsy

# Update ntp clock
echo "Syncing clock with NTP."
sudo ntpdate -u $NTPSERVER && \
    sudo ntpdate -u $NTPSERVER || \
	echo "Failed to update clock via NTP!"
echo "NTP clock sync complete."

exit 0
