#!/bin/bash

EMUBOOT=/var/emulab/boot
REPODIR=/local/repository
SHOUTSRC=$REPODIR/shout

# Do common setup tasks
$REPODIR/bin/init-common.sh

# Add route to VPN gateway host for ME and FE client traffic
sudo ip route add 155.98.47.0/24 via 155.98.36.204

# Run the orchestrator
$SHOUTSRC/orchestrator.py

exit 0
