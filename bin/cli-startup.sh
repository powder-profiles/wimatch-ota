#!/bin/bash

ORCHNAME="orch"

EMUBOOT=/var/emulab/boot
REPODIR=/local/repository
SHOUTSRC=$REPODIR/shout
CLILOG=/var/tmp/ccontroller.log

# Do common setup tasks.
$REPODIR/bin/init-common.sh

# Remove any old log client log file
sudo rm $CLILOG

# Get the FQDN of the orchestrator. Yes, we communicate with it over
# the control network because fixed and mobile endpoints cannot talk
# to it any other way.
ORCHDOM=`$REPODIR/bin/getexpdom.py`
ORCHHOST="$ORCHNAME.$ORCHDOM"

# Fire up the shout measurement client
$SHOUTSRC/meascli.py -s $ORCHHOST

exit 0
