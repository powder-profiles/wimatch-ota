#!/usr/bin/python

"""This profile is intended for capturing WiMatch/Shout dependencies
and for reproducing experiments as conducted for the CNERT 2021
workshop paper.

Allocate some number of X310 radios (+ compute) for doing measurements. 
Can allocate both CBAND and Cellular band X310 Radios as well as FE
NUC+B210 resources.

Instructions:

*Note:* It will not be possible to precisely reproduce the results
presented in the CNERT 2021 paper.  There are two key reasons for this:

  * The measurements were taken in a live over-the-air environment

Third-party incumbents have changed their behaviors and channel
operation since the paper measurements were taken on Nov. 14, 2020.

  * Multiple POWDER radios have been upgraded with high-power front-ends

This drastically changes the power / link budgets as measured by the tools.

Despite these variables, it is still possible to reproduce similar
measurement runs, view link budget charts similar to those in the
paper, and to use the WiMatch query tool to explore resource
requirement satisfiability.

Follow these steps to execute a measurement run with Shout and use
the results to satisfy resource constraint queries:

**1) Instantiate this profile with appropriate parameters**

At the "Parameterize" step, you will want to add as many "Cellular
X310" rooftop radios and "fixed endpoint" radios as are available.
Click on the "Resource Availability" button near the top of the page
to check availability.  Click the "+" symbol to add to the list of
radios to allocate; you'll need a line for each individual site you
want to include.

You will also need to add a "Band 7 cellular" frequency range pair.
Check the bottom of the "Resource availability" page for frequency
ranges that are currently in use, and chose band 7 uplink and downlink
frequencies that are not listed.  For reproducing the WiMatch paper
experiments, the uplink frequency is not used and can just be set to
anything that is free.  Note that the band7 uplink is from 2500 - 2570
MHz, and the downlink is from 2620 - 2690 MHz.  The paper used
frequencies from 2620 - 2640 MHz.

You can leave the other parameters in this profile at their defaults.

Once you have these parameters selected, click through the rest of the
profile and then click "Finish" to instantiate.  It will take 10
to 15 minutes for the experiment to finish setting up.  Once it
is "green", proceed to the next step.

**2) Open SSH sessions**

*Note:* POWDER requires SSH key-based authentication.  You can
upload a key and learn more about using/generating keys by clicking
"Manage SSH Keys" from the top right drop-down menu showing your
username.

As you may be opening quite a number of SSH sessions here, you may
want to look into a multi-terminal session manager like `tmux`,
`Screen`, or another GUI terminal app that supports multiple sessions.
You will also want to have an X session running on your local machine
that you can forward through your SSH sessions.

Open an SSH session to each of the `cellsdr1-<site>-comp`, each of the
`nuc2-<site> nodes, and two sessions on the `orch` node.  You should
forward your X session to one or both of the `orch` ssh sessions,
e.g., with `ssh -Y <username>@<orch_node_hostname>`.

Reference SSH commands can be seen on the "List View" tab next to the
compute node names.  If you have an `ssh://` handler setup on your
browser, you can click these commands to open a corresponding SSH
session (they are hyperlinks).

**3) Start the Shout Orchestrator process**

In one of your `orch` SSH sessions, run:

```
/local/repository/bin/orch-startup.sh
```

This will start the Shout orchestrator that all of the measurement
clients, and command executor script will connect to.

**4) Check radio firmware on x310 (rooftop site) radios**

On each of the `cellsdr1-<site>-comp` nodes, run `uhd_usrp_probe`.  If
the output complains about a firmware mismatch, follow the
instructions shown to sync the firmware.  After any firmware updates,
find the corresponding X310 radio devices in the "List View" on the
POWDER web UI.  Click the checkbox for each device row where a
firmware update was executed, then click the "gear" icon at the top of
the column and select "power cycle selected".  Confirm to complete the
operation and wait about 10 seconds for the devices to come back
online.  Double check that the firmware has updated by running
`uhd_usrp_probe` again.

**5) Start measurement clients**

In the SSH session for each of the `cellsdr1-<site>-comp` and
`<site>-b210` nodes, run: `/local/repository/bin/cli-startup.sh`.  You
should see these clients connect both in the output of the client, and
in the output of the orchestrator.

**6) Execute a measurement run**

*Note:* Before executing the rest of the commands below on the
`orch` host, first change directory to the 'Shout' source repository's
location:

```
cd /local/repository/shout
```

With all clients connected to the orchestrator, you can now perform a
measurement collection run.  There is a JSON command file located
here: `/local/repository/etc/wimatch-run.json`.  If you specified
anything other than 2620 - 2630 MHz when instantiating your
experiment, you will need to change the frequency in this file to
match the bottom of the downlink range you specified.  E.g., if you
specified 2630 - 2640 MHz, then change the frequency to "2630" in this
command file.  You will also want to prune out any TX or RX clients
listed in this file that are not in your experiment to avoid
collecting null data and incurring timeouts.  The command file is
setup to capture measurements every 1 MHz across 10 MHz of bandwidth
starting at the afforementioned starting frequency.  Once the command
file is properly adjusted, execute the following command in your other
`orch` SSH session:

```
./measiface.py -c /local/repository/etc/wimatch-run.json
```

This will kick off the collection run, which should take perhaps 10
minutes to complete.  You can watch the SSH session windows to see the
process progress.  You should see a `measurements.hdf5` file in a new
`mcondata` subdirectory.

**7) Add static radio data to the collected data file**

For constraint solving and resource matching, you will need to add the static radio data for POWDER resources to your dataset.  Execute the following:

```
./static_data.py -i ./data/radio_data.json
```

**8) Check collected/static data and produce graphs**

*Note:* Run your SSH session for these commands such that it forwards
your local window manager session, e.g.: `ssh -Y username@host`

Run `./analyze-data.py -l` to confirm that you have
measurement data and static radio data entries.

Next, run:

```
./analyze-data.py -m -s -d &
```

This will produce graphs of continuous wave power over noise plus
interference versus distance, and power across the 1MHz steps for each
TX/RX pair from the samples collected during the above measurement run.

**9) Execute some resource queries to test constraint solving**

Have a look at the graphs and get an idea of the measured link budgets.
Try out the following query.  You will need to adjust it if it does not
cover the frequency range you specified and used in the measurements.

```
./wimatch/query -q "select 1 of t1, 2 of t2 where t1 is a 'bs', t2 is an 'fe', frequency = 2623, bandwidth = 5, LB(t1 -> t2) > 20;"
```

This query should find a solution unless you have a very small number
of devices in your measurement set.  (You may need to scroll backward
past a bunch of 'lbtable' output to find the matches for `t1_1`,
`t2_1`, etc.)  You can verify from the matched devices in the output
and the bar graph from step 8 above that the base station devices and
fixed endpoint devices have an average link budget of at least 25 dB.
Try raising this value to 30, 35, 40, 45, etc. and see what happens.
You can also try increasing the number of 't1' and 't2' devices
requested to see the effect on finding a satisfiable solution.

"""

# Library imports
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab.pnext as pn
import geni.rspec.emulab.spectrum as spectrum
import geni.rspec.igext as ig

# Global Variables
CBAND_LOW = 3358
CBAND_HIGH = 3600
meas_disk_image = \
        "urn:publicid:IDN+emulab.net+image+emulab-ops:UBUNTU22-64-GR310"
orch_image = meas_disk_image
x310_node_image = meas_disk_image
nuc_image = meas_disk_image
sm_image = meas_disk_image
clisetup_cmd = "/local/repository/bin/cli-startup.sh"
orchsetup_cmd = "/local/repository/bin/orch-startup.sh"
init_cmd = "/local/repository/bin/init-common.sh"

# List of OTA lab X310 radios.
ota_x310_radios = [
    "ota-x310-1",
    "ota-x310-2",
    "ota-x310-3",
    "ota-x310-4",
]

# List of OTA lab NUC+B210 devices.
ota_b210_devices = [
    "ota-nuc1",
    "ota-nuc2",
    "ota-nuc3",
    "ota-nuc4",
]

# List of CBAND rooftop X310 radios.
cband_radios = [
    ("cbrssdr1-bes",
     "Behavioral"),
    ("cbrssdr1-browning",
     "Browning"),
#    ("cbrssdr1-dentistry",
#     "Dentistry"),
    ("cbrssdr1-fm",
     "Friendship Manor"),
    ("cbrssdr1-hospital",
     "Hospital"),
    ("cbrssdr1-honors",
     "Honors"),
    ("cbrssdr1-meb",
     "MEB"),
    ("cbrssdr1-ustar",
     "USTAR"),
]

# List of "Cellular" radios - these are RX-only currently connected
# to a wideband antenna at the respective site.
cell_radios = [
    ("cellsdr1-bes",
     "Behavioral"),
    ("cellsdr1-browning",
     "Browning"),
    ("cellsdr1-hospital",
     "Hospital"),
    ("cellsdr1-meb",
     "MEB"),
]

# A list of fixed endpoint sites.
fe_sites = [
    ('urn:publicid:IDN+bookstore.powderwireless.net+authority+cm',
     "Bookstore"),
    ('urn:publicid:IDN+cpg.powderwireless.net+authority+cm',
     "Garage"),
    ('urn:publicid:IDN+ebc.powderwireless.net+authority+cm',
     "EBC"),
    ('urn:publicid:IDN+guesthouse.powderwireless.net+authority+cm',
     "GuestHouse"),
    ('urn:publicid:IDN+humanities.powderwireless.net+authority+cm',
     "Humanities"),
    ('urn:publicid:IDN+law73.powderwireless.net+authority+cm',
     "Law73"),
    ('urn:publicid:IDN+madsen.powderwireless.net+authority+cm',
     "Madsen"),
    ('urn:publicid:IDN+moran.powderwireless.net+authority+cm',
     "Moran"),
    ('urn:publicid:IDN+sagepoint.powderwireless.net+authority+cm',
     "SagePoint"),
    ('urn:publicid:IDN+web.powderwireless.net+authority+cm',
     "WEB"),
]

# A list of mobile endpoint sites.
me_sites = [
    ("All", "All"),
]

# List of dense radios.
dense_radios = [
    ("cnode-wasatch",
     "Wasatch"),
    ("cnode-mario",
     "Mario"),
    ("cnode-moran",
     "Moran"),
    ("cnode-guesthouse",
     "Guesthouse"),
    ("cnode-ebc",
     "EBC"),
    ("cnode-ustar",
     "USTAR"),
]

# List of PhantomNet devices and links.
pn_devices = [
    "nuc1-nuc2",
    "nuc2-nuc3",
    "nuc3-nuc4",
    "nuc1-nuc4",
    "nuc5-nuc6",
    "nuc1-nuc2-nuc3",
    "nuc1-nuc4-nuc3",
    "nuc1-nuc2-nuc3-nuc4-nuc1"
]

# Top-level request object.
request = portal.context.makeRequestRSpec()

# Node type parameter for PCs to be paired with X310 radios.
# Restricted to those that are known to work well with them.
portal.context.defineParameter(
    "nodetype",
    "Compute node type",
    portal.ParameterType.STRING, "d740",
    ["d740","d430"],
    "Type of compute node to be paired with the X310 Radios",
)

# Node type for the orchestrator.
portal.context.defineParameter(
    "orchtype",
    "Orchestrator node type",
    portal.ParameterType.STRING, "",
    ["", "d430","d740"],
    "Type of compute node for the orchestrator (unset == 'any available')",
)

# Start VNC on all nodes?
portal.context.defineParameter(
    "start_vnc",
    "Start X11 VNC on all compute nodes",
    portal.ParameterType.BOOLEAN, True)

# Set of CBAND X310 radios to allocate
portal.context.defineStructParameter(
    "cband_radio_sites", "CBAND Radio Sites", [],
    multiValue=True,
    min=0,
    multiValueTitle="CBAND X310 radios to allocate.",
    members=[
        portal.Parameter(
            "radio",
            "CBAND Radio Site",
            portal.ParameterType.STRING,
            cband_radios[0], cband_radios,
            longDescription="CBAND X310 radio will be allocated from selected site."
        ),
    ])

# Set of Wideband RX-only radios to allocate
portal.context.defineStructParameter(
    "wideband_radio_sites", "Wideband RX-only sites", [],
    multiValue=True,
    min=0,
    multiValueTitle="Wideband RX-only radios to allocate.",
    members=[
        portal.Parameter(
            "radio",
            "Wideband RX-only radio",
            portal.ParameterType.STRING,
            cell_radios[0], cell_radios,
            longDescription="An RX-only (channel 0, RX2) X310 radio connected to a wideband antenna will be allocated from selected site."
        ),
    ])

# Set of Dense Deployment radio sites to allocate
portal.context.defineStructParameter(
    "dense_radios", "Dense Site Radios", [],
    multiValue=True,
    min=0,
    multiValueTitle="Dense Site NUC+B210 radios to allocate.",
    members=[
        portal.Parameter(
            "device",
            "SFF Compute + NI B210 device",
            portal.ParameterType.STRING,
            dense_radios[0], dense_radios,
            longDescription="A Small Form Factor compute with attached NI B210 device at the given Dense Deployment site will be allocated."
        ),
    ])

# Set of Fixed Endpoint devices to allocate
portal.context.defineStructParameter(
    "fe_radio_sites", "Fixed Endpoint Sites", [],
    multiValue=True,
    min=0,
    multiValueTitle="Fixed Endpoint NUC+B210 radios to allocate.",
    members=[
        portal.Parameter(
            "site",
            "FE Site",
            portal.ParameterType.STRING,
            fe_sites[0], fe_sites,
            longDescription="A `nuc2` device will be selected at the site."
        ),
    ])

# Set of Mobile Endpoint devices to allocate
portal.context.defineStructParameter(
    "me_radio_sites", "Mobile Endpoint Sites", [],
    multiValue=True,
    min=0,
    multiValueTitle="Mobile Endpoint Supermicro+B210 radios to allocate.",
    members=[
        portal.Parameter(
            "site",
            "ME Site",
            portal.ParameterType.STRING,
            me_sites[0], me_sites,
            longDescription="An `ed1` device will be selected at the site, along with its attached B210 radio."
        ),
    ])

# Set of OTA Lab X310 radios to allocate
portal.context.defineStructParameter(
    "ota_lab_x310s", "Indoor OTA Lab Radios", [],
    multiValue=True,
    min=0,
    multiValueTitle="Indoor OTA Lab X310 radios to allocate.",
    members=[
        portal.Parameter(
            "radio",
            "OTA Lab Radio",
            portal.ParameterType.STRING,
            ota_x310_radios[0], ota_x310_radios,
            longDescription="An X310 radio in the indoor OTA Lab along with associated compute node will be allocated."
        ),
    ])

# Set of OTA Lab NUC+B210 devices to allocate
portal.context.defineStructParameter(
    "ota_lab_b210s", "Indoor OTA Lab B210 Devices", [],
    multiValue=True,
    min=0,
    multiValueTitle="Indoor OTA Lab NUC+B210 radios to allocate.",
    members=[
        portal.Parameter(
            "device",
            "NUC+B210 device",
            portal.ParameterType.STRING,
            ota_b210_devices[0], ota_b210_devices,
            longDescription="A NUC+B210 device in the indoor OTA lab will be allocated."
        ),
    ])

# Set of PhantomNet (attenuator matrix) radios to allocate
portal.context.defineStructParameter(
    "phantomnet", "PhantomNet Radios", [],
    multiValue=True,
    min=0,
    max=1,
    multiValueTitle="PhantomNet radios and links to allocate.",
    members=[
        portal.Parameter(
            "device",
            "PhantomNet radios and links",
            portal.ParameterType.STRING,
            pn_devices[0], pn_devices,
            longDescription="PhantomNet (attenuator matrix; controlled RF) radios will be allocated and corresponding links will be created."
        ),
    ])


# Frequency/spectrum parameters
portal.context.defineStructParameter(
    "cband_freq_ranges", "CBAND Frequency Ranges", [],
    multiValue=True,
    min=0,
    multiValueTitle="Frequency ranges for CBAND operation.",
    members=[
        portal.Parameter(
            "freq_min",
            "Frequency Min",
            portal.ParameterType.BANDWIDTH,
            3550.0,
            longDescription="Values are rounded to the nearest kilohertz."
        ),
        portal.Parameter(
            "freq_max",
            "Frequency Max",
            portal.ParameterType.BANDWIDTH,
            3560.0,
            longDescription="Values are rounded to the nearest kilohertz."
        ),
    ])

# Bind and verify parameters
params = portal.context.bindParameters()

for i, frange in enumerate(params.cband_freq_ranges):
    if frange.freq_max - frange.freq_min < 1:
        perr = portal.ParameterError("Minimum and maximum frequencies must be separated by at least 1 MHz", ["cband_freq_ranges[%d].freq_min" % i, "cband_freq_ranges[%d].freq_max" % i])
        portal.context.reportError(perr)
    if frange.freq_min < CBAND_LOW or frange.freq_max > CBAND_HIGH:
        perr = portal.ParameterError("CBAND frequencies must be between %d and %d MHz" % (CBAND_LOW, CBAND_HIGH), ["cband_freq_ranges[%d].freq_min" % i, "cband_freq_ranges[%d].freq_max" % i])
        portal.context.reportError(perr)

# Now verify.
portal.context.verifyParameters()

# Helper function that allocates a PC + X310 radio pair, with Ethernet
# link between them.
def x310_node_pair(x310_radio_name, inparams):
    node = request.RawPC("%s-comp" % x310_radio_name)
    node.hardware_type = inparams.nodetype
    node.disk_image = x310_node_image
    node.addService(rspec.Execute(shell="bash", command=init_cmd))
    if inparams.start_vnc:
        node.startVNC()

    radio = request.RawPC("%s-radio" % x310_radio_name)
    radio.component_id = x310_radio_name

    radio_link = request.Link("%s-link" % x310_radio_name)
    node_radio_if = node.addInterface("usrp_if")
    node_radio_if.addAddress(rspec.IPv4Address("192.168.40.1", "255.255.255.0"))
    radio_link.addInterface(node_radio_if)
    radio_link.addNode(radio)

# Declare that we may be starting X11 VNC on the compute nodes.
if params.start_vnc:
    request.initVNC()
    
# Allocate orchestrator node
orch = request.RawPC("orch")
orch.disk_image = orch_image
orch.hardware_type = params.orchtype
orch.addService(rspec.Execute(shell="bash", command=init_cmd))
if params.start_vnc:
    orch.startVNC()

# Request PC + CBAND X310 resource pairs.
for rsite in params.cband_radio_sites:
    x310_node_pair(rsite.radio, params)

# Request PC + Wideband RX-only radios
for rsite in params.wideband_radio_sites:
    x310_node_pair(rsite.radio, params.nodetype, orch.name)

# Request nuc2+B210 radio resources at FE sites.
for fesite in params.fe_radio_sites:
    nuc = ""
    for urn,sname in fe_sites:
        if urn == fesite.site:
            nuc = request.RawPC("%s-b210" % sname)
            break
    nuc.component_manager_id = fesite.site
    nuc.component_id = "nuc2"
    nuc.disk_image = nuc_image
    nuc.addService(rspec.Execute(shell="bash", command=init_cmd))
    if params.start_vnc:
        nuc.startVNC()

# Request ed1+B210 radio resources at ME sites.
for mesite in params.me_radio_sites:
    if mesite.site == "All":
        obj = request.requestAllRoutes()
        obj.disk_image = nuc_image
        obj.addService(rspec.Execute(shell="bash", command=init_cmd))
        if params.start_vnc:
            obj.startVNC()

# Request NUC+B210 radio resources at the requested Dense Deployment sites.
for dev in params.dense_radios:
    node = request.RawPC("%s-dd-b210" % dev.device)
    node.component_id = dev.device
    node.disk_image = sm_image
    node.addService(rspec.Execute(shell="bash", command=init_cmd))
    if params.start_vnc:
        node.startVNC()

# Request PC + OTA Lab X310 resource pairs.
for dev in params.ota_lab_x310s:
    x310_node_pair(dev.radio, params)

# Request NUC+B210 radio resources in the OTA Lab.
for dev in params.ota_lab_b210s:
    node = request.RawPC("%s-b210" % dev.device)
    node.component_id = dev.device
    node.disk_image = sm_image
    node.addService(rspec.Execute(shell="bash", command=init_cmd))
    if params.start_vnc:
        node.startVNC()

# Request PhantomNet radios
for dev in params.phantomnet:
    node_names = dev.device.split('-')
    n_nodes = len(node_names)
    if node_names[0] == node_names[-1]: # for nuc1-nuc2-nuc3-nuc4-nuc1
        node_names = node_names[:-1]
    
    nodes = []
    for node_name in node_names:
        node = request.RawPC(node_name)
        node.hardware_type = "nuc5300"
        node.component_id = node_name
        node.disk_image = meas_disk_image
        node.addService(rspec.Execute(shell="bash", command=init_cmd))
        if params.start_vnc:
            node.startVNC()
        nodes.append(node)


    node0rf0 = nodes[0].addInterface("n0rf0")
    node1rf0 = nodes[1].addInterface("n1rf0")  

    rflink0 = request.RFLink("rflink0")
    rflink0.addInterface(node0rf0)
    rflink0.addInterface(node1rf0)

    if n_nodes > 2:
        node1rf1 = nodes[1].addInterface("n1rf1")
        node2rf0 = nodes[2].addInterface("n2rf0")

        rflink1 = request.RFLink("rflink1")
        rflink1.addInterface(node1rf1)
        rflink1.addInterface(node2rf0)

    if n_nodes > 3:
        node0rf1 = nodes[0].addInterface("n0rf1")
        node2rf1 = nodes[2].addInterface("n2rf1")
        node3rf0 = nodes[3].addInterface("n3rf0")
        node3rf1 = nodes[3].addInterface("n3rf1")

        rflink2 = request.RFLink("rflink2")
        rflink2.addInterface(node2rf1)
        rflink2.addInterface(node3rf0)

        rflink3 = request.RFLink("rflink3")
        rflink3.addInterface(node3rf1)
        rflink3.addInterface(node0rf1)

# Request frequency range(s)
for frange in params.cband_freq_ranges:
    request.requestSpectrum(frange.freq_min, frange.freq_max, 0)

# Emit!
portal.context.printRequestRSpec()
